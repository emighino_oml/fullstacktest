export default {
    initFilms(state, films) {
        for (let i = 0; i < films.length; i++) {
            var film = films[i];
            film['id'] = i + 1
        }
        state.films = films;
    },
    initFilmDetail (state, film = null) {
        if (film) {
            state.film = film;
        }
    }
};
