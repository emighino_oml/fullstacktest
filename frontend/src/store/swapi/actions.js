/* eslint-disable no-unused-vars */
import SWAPService from '@/services/swap_service';
const service = new SWAPService();

export default {
    async initFilms({ commit }) {
        const { results } = await service.films();
        commit('initFilms', results);
    },
    async initFilmDetail({ commit }, id = null) {
        if (id) {
            var resp = await service.filmDetail(id);
            const { planets, starships, characters } = resp;
            resp['planets'] = []
            resp['starships'] = []
            resp['characters'] = []
            planets.forEach(async p => {
                resp['planets'].push(await service.planetDetail(p));
            });
            starships.forEach(async p => {
                resp['starships'].push(await service.starshipDetail(p));
            });
            characters.forEach(async p => {
                resp['characters'].push(await service.characterDetail(p));
            });
            commit('initFilmDetail', resp);
        }
    }
};
