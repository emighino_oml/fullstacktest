import { createStore } from 'vuex'
import SwapiActions from './swapi/actions';
import SwapiMutations from './swapi/mutations';
import SwapiState from './swapi/state';

export default createStore({
  state: {
    ...SwapiState
  },
  mutations: {
    ...SwapiMutations
  },
  actions: {
    ...SwapiActions
  }
})
