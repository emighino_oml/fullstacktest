/* eslint-disable no-console */
import urls from '@/urls/api-urls';

export default class SWAPService {
    async films () {
        try {
            const resp = await fetch(urls.Films);
            return await resp.json();
        } catch (error) {
            console.error(`Error al listar < Films >`);
            return [];
        }
    }

    async filmDetail (id) {
        try {
            const resp = await fetch(urls.FilmDetail(id));
            return await resp.json();
        } catch (error) {
            console.error(`Error al obtener detalle del Film < ${id} >`);
            return [];
        }
    }

    async planetDetail (url) {
        try {
            const resp = await fetch(url);
            return await resp.json();
        } catch (error) {
            console.error(`Error al obtener detalle del Planeta`);
            return [];
        }
    }
    
    async starshipDetail (url) {
        try {
            const resp = await fetch(url);
            return await resp.json();
        } catch (error) {
            console.error(`Error al obtener detalle de la Nave`);
            return [];
        }
    }
    
    async characterDetail (url) {
        try {
            const resp = await fetch(url);
            return await resp.json();
        } catch (error) {
            console.error(`Error al obtener detalle del Personaje`);
            return [];
        }
    }
}
