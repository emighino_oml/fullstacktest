import { createApp } from 'vue'
import store from '@/store';
import router from '@/router';
import App from './App.vue';
// import * as dotenv from 'dotenv'
// dotenv.config()
require('dotenv').config()

import VueSweetalert2 from 'vue-sweetalert2';

// Primevue
import PrimeVue from 'primevue/config';
import 'primeflex/primeflex.css';
import 'primevue/resources/themes/bootstrap4-dark-blue/theme.css'; // theme
import 'primevue/resources/primevue.min.css'; // core css
import 'primeicons/primeicons.css'; // icons

// Sweetalert Vue
import 'sweetalert2/dist/sweetalert2.min.css';

// Components primevue
import Card from 'primevue/card';
import Button from 'primevue/button';
import InputText from 'primevue/inputtext';
import Tooltip from 'primevue/tooltip';
import Toolbar from 'primevue/toolbar';
import Calendar from 'primevue/calendar';
import ToastService from 'primevue/toastservice';
import Slider from 'primevue/slider';
import SpeedDial from 'primevue/speeddial';
import SplitButton from 'primevue/splitbutton';
import Carousel from 'primevue/carousel';
import Menubar from 'primevue/menubar';

const app = createApp(App);

app.directive('tooltip', Tooltip);
app.component('Card', Card);
app.component('Button', Button);
app.component('InputText', InputText);
app.component('Toolbar', Toolbar);
app.component('Calendar', Calendar);
app.component('Slider', Slider);
app.component('SpeedDial', SpeedDial);
app.component('SplitButton', SplitButton);
app.component('Carousel', Carousel);
app.component('Menubar', Menubar);

app.use(store)
    .use(router)
    .use(VueSweetalert2)
    .use(PrimeVue)
    .use(ToastService)
    .mount('#app');
