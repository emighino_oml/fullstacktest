const BASE_URL = 'https://swapi.dev/api';

export default {
    Films: `${BASE_URL}/films`,
    FilmDetail: (id) => `${BASE_URL}/films/${id}`,
    CharacterDetail: (id) => `${BASE_URL}/people/${id}`,
    StarshipDetail: (id) => `${BASE_URL}/starships/${id}`,
    PlanetDetail: (id) => `${BASE_URL}/planets/${id}`
};
