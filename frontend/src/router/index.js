import { createRouter, createWebHistory } from 'vue-router'
import Register from '@/views/Register'
import Films from '@/views/Films'
import FilmDetail from '@/views/FilmDetail'

const routes = [
  {
    path: '/',
    name: 'films',
    component: Films
  },
  {
    path: '/film/:id/detail',
    name: 'film_detail',
    component: FilmDetail
  },
  {
    path: '/register',
    name: 'register',
    component: Register
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
