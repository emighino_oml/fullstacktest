# full_stack_test

## Instalar Docker
```
https://docs.docker.com/desktop/install/ubuntu/
```

### Crear la imagen de Vue APP
```
docker build -t vuejs-app-img .
```

### Correr el container de la app
```
docker run -dit -p 7777:80 --name vuejs-app-1 vuejs-app-img
```

### Ir al navegador
```
http://localhost:7777/
```