
import mongoose from 'mongoose';
import { MONGO, MONGO_URI } from "./../const";

export const connectDB = async () => {
    await mongoose.connect(MONGO_URI, {
        authSource: MONGO.AUTH_SOURCE,
        user: MONGO.USER,
        pass: MONGO.PASSWORD
    });
    console.log('---> MongoDB Connected');
}