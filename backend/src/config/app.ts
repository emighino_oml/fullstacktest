import express, {Request, Response, json, urlencoded, Application} from "express";
import cors from "cors";
import morgan from 'morgan';
import helmet from "helmet";
import compression from "compression";
import { config } from "dotenv";
import { connectDB } from "./db";
import { userRoutes } from "../routes/user_routes";
import { transitRoutes } from "../routes/transit_routes";
import { authRoutes } from "../routes/auth_routes";
import { handleError } from "../middleware/error_handler_middleware";
import { validateAccess } from "./../utils/jwt";

class App {
    public app: Application;

    constructor() {
        this.app = express();
        this.setEnvVars();
        this.setMongoConfig();
        this.setConfig();
        this.setRoutes();
        this.setErrorHandlingMiddleware();
    }

    private setEnvVars(): void {
        if (process.env.NODE_ENV == 'production' ) {
            config()
        } else if (process.env.NODE_ENV == 'developmen' ) {
            config({path: 'dev.env'})
        } else if (process.env.NODE_ENV == 'test' ) {
            config({path: 'test.env'})
        }
    }

    private setConfig(): void {
        this.app.use(json());
        this.app.use(urlencoded({ extended: false }));
        this.app.use(cors());
        this.app.use(helmet());
        this.app.use(compression());
        this.app.use(morgan(':method :url :status :res[content-length] - :response-time ms'))
    }

    private setRoutes(): void {
        this.app.use('/auth', authRoutes.router);
        this.app.use('/users', validateAccess, userRoutes.router);
        this.app.use('/transit', validateAccess, transitRoutes.router);
        this.app.all('*', (_: Request, res: Response) => {
            res.status(404).send({ status: 'ERROR', message: 'URL not found' });
        });
    }

    private async setMongoConfig(): Promise<void> {
        await connectDB();
    }

    private setErrorHandlingMiddleware(): void {
        this.app.use(handleError);
    }
}

export default new App().app;
