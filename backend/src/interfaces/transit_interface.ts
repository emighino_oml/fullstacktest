import { Document } from "mongoose";

export interface ITransit extends Document {
  fecha_evento: Date;
  hora: string;
  tipo_de_evento: string;
  coordenada_x: Number;
  coordenada_y: Number;
  colonia: string;
  alcaldia: string;
  tipo_de_interseccion: string;
  // tipo_de_vehiculo_1: string;
  // tipo_de_vehiculo_2: string;
  // tipo_de_vehiculo_3: string;
  // tipo_de_vehiculo_4: string;
  // marca_de_vehiculo_1: string;
  // marca_de_vehiculo_2: string;
  // marca_de_vehiculo_3: string;
  // marca_de_vehiculo_4: string;
  // unidad_medica_de_apoyo: string;
  // prioridad: string;
  // color_vehiculo_1: string;
  // color_vehiculo_2: string;
  // color_vehiculo_3: string;
}