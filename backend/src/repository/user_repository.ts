import { BaseRepository } from "./base_repository";
import { User } from "./../models/user_model";

export class UserRepository extends BaseRepository {
    constructor () {
        super(User);
    }
}