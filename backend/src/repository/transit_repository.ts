import { BaseRepository } from "./base_repository";
import { Transit } from "./../models/transit_model";
import { ITransit } from "./../interfaces/transit_interface";

export class TransitRepository extends BaseRepository {
    constructor() {
        super(Transit);
    }

    public async lastN(n: Number): Promise<ITransit[]> {
        return await this.model.find().sort({ $natural: -1 }).limit(n);
    }

    public async alcaldias_con_mas_choques() {
        var results = [];
        const alcaldias = await this.model.distinct('alcaldia');
        for (let i = 0; i < alcaldias.length; i++) {
            const alcaldia = alcaldias[i];
            const n = await this.model.find({ tipo_de_evento: 'CHOQUE', alcaldia }).count();
            results.push({ alcaldia, num: n});
        }
        return results.sort((a, b) => b.num - a.num).slice(0, 10);
    }

    public async marcas_con_mas_choques() {
        var results = [];
        const marcas = await this.model.distinct('marca_de_vehiculo_1');
        for (let i = 0; i < marcas.length; i++) {
            const marca = marcas[i];
            const n = await this.model.find({ tipo_de_evento: 'CHOQUE', marca_de_vehiculo_1: marca }).count();
            results.push({ marca, num: n});
        }
        return results.sort((a, b) => b.num - a.num).slice(0, 10);
    }
}