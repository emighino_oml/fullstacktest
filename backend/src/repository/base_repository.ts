import { IUser } from "./../interfaces/user_interface";
import { ITransit } from "./../interfaces/transit_interface";

export class BaseRepository {
    protected model = null;

    constructor (model) {
        this.model = model;
    }

    public async list (): Promise<ITransit[] | IUser[]> {
        return await this.model.find();
    }

    public async retrive (id: string): Promise<ITransit | IUser> {
        return await this.model.findById(id);
    }

    public async create (entity: IUser | ITransit) {
        return await this.model.create(entity);
    }

    public async update (id: string, entity: IUser | ITransit) {
        return await this.model.findByIdAndUpdate(id, entity);
    }

    public async delete (id: string) {
        return await this.model.findByIdAndDelete(id);
    }
}