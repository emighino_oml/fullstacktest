import { compare, hash } from "bcryptjs";
import { sign } from 'jsonwebtoken';
import { CustomError } from "./../errors/custom_error";
import { HTTP_STATUS } from "./../const";
import { User } from "./../models/user_model";
import { JWT_SECRET, JWT_TOKEN_DURATION, JWT_SALT } from "./../const";
import { IUser } from "./../interfaces/user_interface";

export class AuthService {

    private async verifyUserLogin(email: string, password: string) {
        var user: IUser = await User.findOne({ email });
        if (!user) {
            throw new CustomError('User not found', HTTP_STATUS.NOT_FOUND);
        }
        if (await compare(password, user.password)) {
            const token = sign({ id: user._id, username: user.username, email: user.email }, JWT_SECRET, { expiresIn: `${JWT_TOKEN_DURATION}h` })
            return { status: 'OK', token }
        }
        throw new CustomError('Invalid password', HTTP_STATUS.BAD_REQUEST);
    }

    public async login(user: Partial<IUser>) {
        const { email, password } = user;
        if (!email || !password) {
            throw new CustomError('Email and password are required', HTTP_STATUS.BAD_REQUEST);
        }
        return await this.verifyUserLogin(email, password);
    }

    public async signup(user: IUser) {
        try {
            const { password, email, role, username } = user;
            const hashedPwd = await hash(password, JWT_SALT);
            const result = await User.create({
                email,
                role,
                username,
                password: hashedPwd
            });
            return result;
        } catch (error) {
            throw new CustomError(error, HTTP_STATUS.SERVER_ERROR);
        }
    }
}