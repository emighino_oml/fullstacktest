import { Types } from "mongoose";
import { IUser } from "../interfaces/user_interface";
import { ITransit } from "../interfaces/transit_interface";
import { CustomError } from "./../errors/custom_error";
import { HTTP_STATUS } from "./../const";

export class BaseService {
    protected repository = null;

    constructor (repository) {
        this.repository = repository;
    }

    private validateId (id: string) {
        if (!id) {
            throw new CustomError('ID is required', HTTP_STATUS.BAD_REQUEST);
        } else if (!Types.ObjectId.isValid(id)) {
            throw new CustomError('Invalid ID field', HTTP_STATUS.BAD_REQUEST);
        }        
    }

    public async list(): Promise<IUser[]> {
        try {
            return await this.repository.list();
        } catch (error) {
            throw new CustomError(error._message, HTTP_STATUS.SERVER_ERROR, error.errors);
        }
    }

    public async create(user: IUser | ITransit) {
        try {
            return await this.repository.create(user);
        } catch (error) {
            throw new CustomError(error._message, HTTP_STATUS.SERVER_ERROR, error.errors);
        }
    }

    public async delete(id: string) {
        this.validateId(id);
        const deletedObj = await this.repository.delete(id);
        if (!deletedObj) {
            throw new CustomError('Not found', HTTP_STATUS.NOT_FOUND, `Object with id '${id}' not found`);
        }
        return deletedObj;
    }

    public async retrive(id: string) {
        this.validateId(id);
        const obj = await this.repository.retrive(id);
        if (!obj) {
            throw new CustomError('Not found', HTTP_STATUS.NOT_FOUND, `Object with id '${id}' not found`);
        }
        return obj;
    }

    public async update(id: string, user: IUser | Partial<IUser> | ITransit | Partial<ITransit>) {
        this.validateId(id);
        const updatedObj = await this.repository.update(id, user);
        if (!updatedObj) {
            throw new CustomError('Not found', HTTP_STATUS.NOT_FOUND, `Object with id '${id}' not found`);
        }
        return updatedObj;
    }
}