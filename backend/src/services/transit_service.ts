import { TransitRepository } from "./../repository/transit_repository";
import { BaseService } from "./base_service";
import { ITransit } from "./../interfaces/transit_interface";
import { CustomError } from "./../errors/custom_error";
import { HTTP_STATUS } from "./../const";

export class TransitService extends BaseService {
    constructor() {
        super(new TransitRepository());
    }

    public async lastN(n: Number): Promise<ITransit[]> {
        try {
            return await this.repository.lastN(n);
        } catch (error) {
            throw new CustomError(error._message, HTTP_STATUS.SERVER_ERROR, error.errors);
        }
    }

    public async alcaldias_con_mas_choques() {
        try {
            return await this.repository.alcaldias_con_mas_choques();
        } catch (error) {
            throw new CustomError(error._message, HTTP_STATUS.SERVER_ERROR, error.errors);
        }
    }

    public async marcas_con_mas_choques() {
        try {
            return await this.repository.marcas_con_mas_choques();
        } catch (error) {
            throw new CustomError(error._message, HTTP_STATUS.SERVER_ERROR, error.errors);
        }
    }
}