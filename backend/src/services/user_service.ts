import { UserRepository } from "./../repository/user_repository";
import { BaseService } from "./base_service";

export class UserService extends BaseService {
    constructor() {
        super(new UserRepository());
    }

    public getHelloMsg(): string {
        return 'Hola Mundo';
    }
}