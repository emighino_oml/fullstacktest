
import app from "./config/app";
import { PORT } from "./const";

app.listen(PORT, () => console.log(`\n\n\n---> Listening on port ${PORT}`));