import { JWT_SECRET } from "./../const";
import { Request, Response, NextFunction } from "express";
import { verify } from 'jsonwebtoken';

export const validateAccess = (req: Request, res: Response, next: NextFunction) => {
  try {
    const { authorization } = req.headers
    const token = authorization.split(" ")[1]
    if (token) {
      verify(token, JWT_SECRET, (err, user) => {
        if (err) {
          res.status(403).send({ status: 'ERROR', message: 'Token invalid' });
        } else {
          req.user = user;
          next();
        }
      })
    } else {
      res.status(400).send({ status: 'ERROR', message: 'Token is required' });
    }
  } catch (error) {
    res.status(400).send({ status: 'ERROR', message: 'Token is required' });
  }
}