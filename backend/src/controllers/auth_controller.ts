import { Request, Response, NextFunction } from "express";
import { AuthService } from "../services/auth_service";
import { JWT_TOKEN_DURATION } from "./../const";
import { IUser } from "./../interfaces/user_interface";


export class AuthController {
    private service: AuthService = null;

    constructor() {
        this.service = new AuthService();
    }

    public login = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const result = await this.service.login(req.body);
            if (result.status === 'OK') {
                res.status(200).send(result);
            } else {
                res.status(500).send(result);
            }
        } catch (error) {
            next(error);
        }
    };

    public signup = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const data: IUser = req.body;
            const result = await this.service.signup(data);
            res.status(201).send({ status: 'OK', message: "User created", data: result });
        } catch (error) {
            next(error);
        }
    };
}