import { Request, Response, NextFunction } from "express";
import { IUser } from "../interfaces/user_interface";
import { ITransit } from "../interfaces/transit_interface";

export class BaseController {
    protected service = null;

    constructor(service) {
        this.service = service;
    }

    public check = (_: Request, res: Response) => {
        res.status(200).json({ msg: "API Ok" });
    }

    public list = async (_: Request, res: Response, next: NextFunction) => {
        try {
            const result: Array<ITransit | IUser> = await this.service.list();
            res.send(result);
        } catch (error) {
            next(error);
        }
    };

    public create = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const result: ITransit | IUser = await this.service.create(req.body);
            res.status(200).send(result);
        } catch (error) {
            next(error);
        }
    };

    public delete = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const result = await this.service.delete(req.params.id);
            res.status(200).send(result);
        } catch (error) {
            next(error);
        }
    };

    public retrive = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const result: ITransit | IUser = await this.service.retrive(req.params.id);
            res.status(200).send(result);
        } catch (error) {
            next(error);
        }
    };

    public update = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const result: ITransit | IUser = await this.service.update(
                req.params.id,
                req.body
            );
            res.status(200).send(result);
        } catch (error) {
            next(error);
        }
    };
}