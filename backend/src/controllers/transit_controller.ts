import { Request, Response, NextFunction } from "express";
import { ITransit } from "../interfaces/transit_interface";
import { TransitService } from "../services/transit_service";
import { BaseController } from "./base_controller";

export class TransitController extends BaseController {
    constructor() {
        super(new TransitService());
    }

    public lastN = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { n } = req.params;
            const result: Array<ITransit> = await this.service.lastN(n);
            res.send(result);
        } catch (error) {
            next(error);
        }
    };

    public alcaldias_con_mas_choques = async (_: Request, res: Response, next: NextFunction) => {
        try {
            const result = await this.service.alcaldias_con_mas_choques();
            res.send(result);
        } catch (error) {
            next(error);
        }
    };

    public marcas_con_mas_choques = async (_: Request, res: Response, next: NextFunction) => {
        try {
            const result = await this.service.marcas_con_mas_choques();
            res.send(result);
        } catch (error) {
            next(error);
        }
    };
}