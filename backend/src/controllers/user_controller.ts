import { Request, Response, NextFunction } from "express";
import { UserService } from "../services/user_service";
import { BaseController } from "./base_controller";

export class UserController extends BaseController {
    constructor() {
        super(new UserService());
    }
}