import { ITransit } from "../interfaces/transit_interface";
import { model, Schema } from "mongoose";

const TransitSchema = new Schema({
  fecha_evento: { type: Date },
  hora: { type: Date },
  tipo_de_evento: { 
    type: String,
    required: true,
    enum: {
      values: [
        'ATROPELLADO',
        'CAIDA DE CICLISTA',
        'CAIDA DE PASAJERO',
        'CHOQUE',
        'DERRAPADO',
        'VOLCADURA'
      ],
      message: '{VALUE} is not supported'
    }
  },
  coordenada_x: { type: Number, required: true },
  coordenada_y: { type: Number, required: true },
  colonia: { type: String, required: true },
  alcaldia: { type: String, required: true },
  tipo_de_interseccion: {
    type: String,
    required: true,
    enum: {
      values: [
        'CRUZ',
        'CURVA',
        'DESNIVEL',
        'GAZA',
        'GLORIETA',
        'RAMAS MULTIPLES',
        'RECTA',
        'T',
        'Y'
      ],
      message: '{VALUE} is not supported'
    }
  }
  // tipo_de_vehiculo_1: { type: String },
  // tipo_de_vehiculo_2: { type: String },
  // tipo_de_vehiculo_3: { type: String },
  // tipo_de_vehiculo_4: { type: String },
  // marca_de_vehiculo_1: { type: String },
  // marca_de_vehiculo_2: { type: String },
  // marca_de_vehiculo_3: { type: String },
  // marca_de_vehiculo_4: { type: String },
  // unidad_medica_de_apoyo: { type: String },
  // prioridad: { type: String },
  // color_vehiculo_1: { type: String },
  // color_vehiculo_2: { type: String },
  // color_vehiculo_3: { type: String }
}, {
  timestamps: true
});


export const Transit = model<ITransit>("Transit", TransitSchema);