import { IUser } from "../interfaces/user_interface";
import { model, Schema } from "mongoose";

const UserSchema = new Schema({
  username: { type: String, required: [true, "Username is required"], unique: true },
  email: { type: String, required: [true, "Email is required"], unique: true },
  password: { type: String, required: [true, "Password is required"], min: 6, max: 30 },
  role: {
    type: String,
    required: true,
    enum: {
      values: [
        'ADMIN',
        'USER'
      ],
      message: '{VALUE} is not supported'
    },
    default: 'USER'
  }
}, {
  timestamps: true
});


export const User = model<IUser>("User", UserSchema);