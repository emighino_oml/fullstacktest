require('dotenv').config();
export const PORT = process.env.PORT || 7778;
export const JWT_SECRET = process.env.JWT_SECRET || '3m1g0nh123..';
export const JWT_TOKEN_DURATION = 2;
export const JWT_SALT = 12;
export const MONGO = {
    AUTH_SOURCE: 'admin',
    PORT: process.env.MONGO_PORT || 27027,
    HOST: process.env.MONGO_HOST || 'localhost',
    USER: process.env.MONGO_USERNAME || 'emighino',
    PASSWORD: process.env.MONGO_PASSWORD || 'emigonh123..',
    DATABASE: process.env.MONGO_DATABASE || 'stack_mean_mongo_db',
}
export const MONGO_URI =  `mongodb://${MONGO.HOST}:${MONGO.PORT}/${MONGO.DATABASE}`;
export const HTTP_STATUS = {
    OK: 200,
    CREATED: 201,
    BAD_REQUEST: 400,
    FORBIDDEN: 403,
    NOT_FOUND: 404,
    METHOD_NOT_ALLOWED: 405,
    SERVER_ERROR: 500
}