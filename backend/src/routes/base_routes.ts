import { Router } from "express";

export class BaseRoutes {
    public router = Router();
    protected controller = null;

    constructor (controller) {
        this.controller = controller;
        this.setRoutes();
    }

    private setRoutes() {
        // this.router.route('/health_check').get(this.controller.check);
        this.router.route("/").get(this.controller.list);
        this.router.route("/").post(this.controller.create);
        this.router.route("/:id/detail").get(this.controller.retrive);
        this.router.route("/:id")
            .delete(this.controller.delete)
            .put(this.controller.update);
    }
}