import { TransitController } from "./../controllers/transit_controller";
import { BaseRoutes } from "./base_routes";

class TransitRoutes extends BaseRoutes {
    constructor () {
        super(new TransitController());
        this.setCustomRoutes();
    }

    private setCustomRoutes (): void {
        this.router.route("/ultimos/:n").get(this.controller.lastN);
        this.router.route("/alcaldias_con_mas_choques").get(this.controller.alcaldias_con_mas_choques);
        this.router.route("/marcas_con_mas_choques").get(this.controller.marcas_con_mas_choques);
    }
}

export const transitRoutes = new TransitRoutes();