import { AuthController } from "./../controllers/auth_controller";
import { Router } from "express";

class AuthRoutes {
    public router: Router = null;
    private controller: AuthController = null;

    constructor () {
        this.router = Router();
        this.controller = new AuthController();
        this.setCustomRoutes();
    }

    private setCustomRoutes (): void {
        this.router.route("/login").post(this.controller.login);
        this.router.route("/signup").post(this.controller.signup);
    }
}

export const authRoutes = new AuthRoutes();