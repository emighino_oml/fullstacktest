import { UserController } from "./../controllers/user_controller";
import { BaseRoutes } from "./base_routes";

class UserRoutes extends BaseRoutes {
    constructor () {
        super(new UserController());
    }
}

export const userRoutes = new UserRoutes();